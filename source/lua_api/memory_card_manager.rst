MemoryCardManager
=================

|since_itg|

.. contents:: :local:

Description
-----------

Some stuff to do with memory card interaction. May or may not be accessible through the :lua:attr:`MEMCARDMAN
<global.global.MEMCARDMAN>` singleton.

API reference
-------------

.. lua:autoclass:: MemoryCardManager
