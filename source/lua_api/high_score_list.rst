HighScoreList
=============

|since_itg|

.. contents:: :local:

Description
-----------

A list of :lua:class:`HighScore` objects

API reference
-------------

.. lua:autoclass:: HighScoreList
