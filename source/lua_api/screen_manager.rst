ScreenManager
=============

|since_itg|

.. contents:: :local:

Description
-----------

The screen manager, accessible from the global singleton :lua:attr:`SCREENMAN <global.global.SCREENMAN>`

API reference
-------------

.. lua:autoclass:: ScreenManager
