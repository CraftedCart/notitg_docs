Globals
=======

.. contents:: :local:

Description
-----------

Global constants and singletons are accessible from anywhere in Lua.

This list is not complete - there are also globals for enumeration variants. See :doc:`/lua_api/enumerations` for a list
of those constants (Note that only UPPERCASE enum variants have global constants).

API reference
-------------

.. lua:automodule:: global
