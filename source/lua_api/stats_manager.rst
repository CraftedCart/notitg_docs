StatsManager
============

|since_itg|

.. contents:: :local:

Description
-----------

The stats manager, accessible from the global :lua:attr:`STATSMAN <global.global.STATSMAN>`

API reference
-------------

.. lua:autoclass:: StatsManager
