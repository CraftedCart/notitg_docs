ProfileManager
==============

|since_itg|

.. contents:: :local:

Description
-----------

The profile manager, accessible from the global :lua:attr:`PROFILEMAN <global.global.PROFILEMAN>`

API reference
-------------

.. lua:autoclass:: ProfileManager
