Profile
=======

|since_itg|

.. contents:: :local:

Description
-----------

A player or machine profile

API reference
-------------

.. lua:autoclass:: Profile
