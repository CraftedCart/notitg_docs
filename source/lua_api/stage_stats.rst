StageStats
==========

|since_itg|

.. contents:: :local:

Description
-----------

Statistics for a stage

API reference
-------------

.. lua:autoclass:: StageStats
