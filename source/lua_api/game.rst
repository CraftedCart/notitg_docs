Game
====

|since_itg|

.. contents:: :local:

Description
-----------

Represents a game mode

You can get an instance of :lua:class:`Game` with :lua:meth:`GameState.GetCurrentGame`.

API reference
-------------

.. lua:autoclass:: Game
