GameState
=========

|since_itg|

.. contents:: :local:

Description
-----------

The :lua:class:`GameState` singleton, accessible from the global :lua:attr:`GAMESTATE <global.global.GAMESTATE>`

API reference
-------------

.. lua:autoclass:: GameState
