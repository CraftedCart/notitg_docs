Model
=====

|since_itg|

.. contents:: :local:

Description
-----------

An 3D model

XML attributes
--------------

**File**

A filepath to the initial Milkshape 3D ASCII model to load

API reference
-------------

.. lua:autoclass:: Model
