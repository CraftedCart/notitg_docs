PlayerStageStats
================

|since_itg|

.. contents:: :local:

Description
-----------

Statistics for a player for a stage

API reference
-------------

.. lua:autoclass:: PlayerStageStats
