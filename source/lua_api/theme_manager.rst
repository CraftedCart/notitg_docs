ThemeManager
============

|since_itg|

.. contents:: :local:

Description
-----------

The theme manager, accessible from the global :lua:attr:`THEME <global.global.THEME>`

API reference
-------------

.. lua:autoclass:: ThemeManager
