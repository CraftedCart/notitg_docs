Polygon
=======

|since_notitg_v3|

.. contents:: :local:

Description
-----------

An arbitrary polygon mesh

You may want to consider calling :lua:meth:`Actor.zwrite` and :lua:meth:`Actor.ztest` on this one if you're doing
anything in 3D.

API reference
-------------

.. lua:autoclass:: Polygon
